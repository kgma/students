package w04;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Meeting {
	/*
	 * Hmm, her var det noe som mangler
	 */
	Date startTime;
	Date endTime;
	
	public Meeting(Date startTime, Date endTime) {
		if (startTime != null && endTime != null & startTime.before(endTime)) {
			this.startTime = startTime;
			this.endTime = endTime;
		}
		
		// Dette er ikke bra nok, men vi skal se p� hvordan vi kan bedre dette i neste forelesning
		// hint throw exceptions
	}
	
	@Override
	public String toString() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
		return "Meeting [start=" + simpleDateFormat.format(startTime) + ", end=" + simpleDateFormat.format(endTime) + "]";
	}
}
